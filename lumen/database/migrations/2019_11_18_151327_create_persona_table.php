<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('persona', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('apellidos');
            $table->string('numero');




            $table->integer('tipodoc_id');
            $table->foreign('tipodoc_id')->references('id')-> on('catalogo')-> onDelete('cascade')-> onUpdate('cascade');
            $table->integer('tipopersona_id');
            $table->foreign('tipopersona_id')->references('id')-> on('catalogo')-> onDelete('cascade')-> onUpdate('cascade');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('persona');
    }
}
