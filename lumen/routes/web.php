<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use App\Http\Models\Persona;
use App\Http\Models\Catalogo;
use App\Http\Models\Carrera;


$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(["prefix" => "api"], function () use ($router)
{
    $router->get("Persona","PersonaController@index");
    $router->get("Persona/{id}","PersonaController@show");
    $router->post("Persona","PersonaController@create");
    $router->put("Persona/{id}","PersonaController@update");
    $router->delete("Persona/{id}","PersonaController@delete");
    $router->put("Persona/"."salida"."/{id}","PersonaController@finalizar_entrada");
    $router->put("Persona/"."entrada"."/{id}","PersonaController@iniciar_entrada");

});

$router->group(["prefix" => "api"], function () use ($router)
{
    $router->get("Catalogo","CatalogoController@index");
    $router->get("Catalogo/{id}","CatalogoController@show");
    $router->post("Catalogo","CatalogoController@create");
    $router->put("Catalogo/{id}","CatalogoController@update");
    $router->delete("Catalogo/{id}","CatalogoController@delete");

});


$router->group(["prefix" => "api"], function () use ($router)
{
    $router->get("Carrera","CarreraController@index");
    $router->get("Carrera/{id}","CarreraController@show");
    $router->post("Carrera","CarreraController@crearte");
    $router->put("Carrera/{id}","Carreraontroller@update");
    $router->delete("Carrera/{id}","CarreraController@delete");

});

$router->group(["prefix" => "api"], function () use ($router)
{
    $router->get("PersonaCarrera","CarreraController@index");
    $router->get("PersonaCarrera/{id}","CarreraController@show");
    $router->post("PersonaCarrera","CarreraController@crearte");
    $router->put("PersonaCarrera/{id}","Carreraontroller@update");
    $router->delete("PersonaCarrera/{id}","CarreraController@delete");

});

// API route group
$router->group(['prefix' => 'api'], function () use ($router) {
    // Matches "/api/register
    $router->post('register', 'AuthController@register');
    // Matches "/api/login
    $router->post('login', 'AuthController@login');

    // Matches "/api/profile
    $router->get('profile', 'UserController@profile');

    // Matches "/api/users/1
    //get one user by id
    $router->get('users/{id}', 'UserController@singleUser');

    // Matches "/api/users
    $router->get('users', 'UserController@allUsers');
});


