<?php

namespace App\Http\Models;
use Illuminate\Database\Eloquent\Model;

class PersonaCarrera extends Model
{
    protected $table = "personacarrera";
    protected $primaryKey ="id";
    protected $fillable = ["id","id_nombre","id_carrera"];


    public function tipo_percarrera()
    {
        return $this->belongsTo('App\Http\Models\Carrera','id_carrera','id');
    }


}

