<?php

namespace App\Http\Models;
use Illuminate\Database\Eloquent\Model;
class Persona extends Model
{
    protected $table = "persona";
    protected $primaryKey ="id";
    protected $fillable = ["id","nombre","apellidos","numero","tipodoc_id","tipopersona_id","sunedu"];


    /**
     * Get the groups for student.
     */
    public function tipo_persona()
    {
        return $this->belongsTo('App\Http\Models\Catalogo','tipopersona_id','id');
    }
    public function tipo_doc()
    {
        return $this->belongsTo('App\Http\Models\Catalogo','tipodoc_id','id');
    }

    public function tipo_idcarrera()
    {
        return $this->hasmany('App\Http\Models\PersonaCarrera','id_persona','id');
    }

    public function entrada_salida()
    {
        return $this->hasmany('App\Http\Models\EntradaSalida','id','id_persona');
    }

//    public function tipo_percarrera()
//    {
//        return $this->belongsTo('App\Http\Models\PersonaCarrera','id_persona','id');
//    }
//    public function tipo_carrera()
//    {
//        return $this->belongsTo('App\Http\Models\PersonaCarrera','id_carrera','id');
//    }


//    public function tipo_persona()
//    {
//        return $this->belongsTo('App\Http\Models\Catalogo','tipopersona_id','id');
//    }
//    public function tipo_doc()
//    {
//        return $this->belongsTo('App\Http\Models\Catalogo','tipodoc_id','id');
//    }



}

