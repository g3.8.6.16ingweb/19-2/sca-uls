<?php

namespace App\Http\Models;
use Illuminate\Database\Eloquent\Model;
class Departamento extends Model
{
    protected $table = "departamento";
    protected $primaryKey ="id";
    protected $fillable = ["id","nombre"];



}
