<?php


namespace App\Http\Controllers;

use App\Http\Models\Carrera;
use App\Http\Models\Persona;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Exception;
use Laravel\Lumen\Application;
use Symfony\Component\HttpFoundation\Response;

class CarreraController extends BaseController
{
    public function  __construct()
    {
    }

    public function index(Application $app, Request $request){

        $this->getPaginationParameters($request);
        $query = Carrera::orderBy($this->sort , $this->sortDirection);
//        $query ->with(['tipo_persona','tipo_doc']);


        if($request->has('nombre'))
            $query->where('nombre','LiKE','%'.$request->nombre.'%');
        return new JsonResponse($query->paginate($this->limit));

    }

    public function  show(Request $request,$id)
    {
        try{
            $carrera = Carrera::find($id);
            return response()->json($carrera,Response::HTTP_OK);
        }catch (Exception $ex){
            return Response()->json(["error" => "hubo un error al encontrar el  id Carrera " . $id .":". $ex-> getMessage()],404);
        }

    }

    public function  create(Request $request)
    {
        try{
            $carrera = Carrera::create($request -> all());
            return response()->json($carrera,Response::HTTP_CREATED);
        }catch (Exception $ex){
            return Response()->json(["error" => "hubo un error al crear un Carrera ". $ex-> getMessage()],400);
        }

    }

    public function  update(Request $request,$id)
    {
        try{
            $carrera = Carrera::findOrfail($id);
            $carrera -> update($request->all());
            return response()->json($carrera,Response::HTTP_OK);
        }catch (Exception $ex){
            return Response()->json(["error" => "hubo un error al registrar en el Carrera".  $id .":". $ex-> getMessage()],400);
        }

    }

    public function  delete(Request $request,$id)
    {
        try{
            Carrera::find($id) -> delete();
            return response()->json([],Response::HTTP_OK);
        }catch (Exception $ex){
            return Response()->json(["error" => "hubo un error a eliminar lista en el Carrera".  $id .":". $ex-> getMessage()],400);
        }

    }





}
