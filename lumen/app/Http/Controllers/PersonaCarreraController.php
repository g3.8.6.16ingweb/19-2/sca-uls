<?php


namespace App\Http\Controllers;

use App\Http\Models\Persona;
use App\Http\Models\PersonaCarrera;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Exception;
use Laravel\Lumen\Application;
use Symfony\Component\HttpFoundation\Response;


class PersonaCarreraController extends BaseController {
    public function  __construct()
    {
    }


    public function index(Application $app, Request $request){

        $this->getPaginationParameters($request);
        $query = PersonaCarrera($this->sort , $this->sortDirection);
        $query ->with(['tipo_percarrera']);
//        with(['tipo_percarrera'])->


        if($request->has('nombre'))
            $query->where('nombre','LiKE','%'.$request->nombre.'%');
        return new JsonResponse($query->paginate($this->limit));

    }

    public function  show(Request $request,$id)
    {
        try{
            $carreraper = PersonaCarrera::with(['tipo_percarrera'])->find($id);
            return response()->json($carreraper,Response::HTTP_OK);
        }catch (Exception $ex){
            return Response()->json(["error" => "hubo un error al encontrar el  id carrera " . $id .":". $ex-> getMessage()],404);
        }

    }

    public function  create(Request $request)
    {
        try{
            $carreraper = PersonaCarrera::create($request -> all());
            return response()->json($carreraper,Response::HTTP_CREATED);
        }catch (Exception $ex){
            return Response()->json(["error" => "hubo un error al crear un persona carrera ". $ex-> getMessage()],400);
        }

    }

    public function  update(Request $request,$id)
    {
        try{
            $carreraper = PersonaCarrera::findOrfail($id);
            $carreraper -> update($request->all());
            return response()->json($carreraper,Response::HTTP_OK);
        }catch (Exception $ex){
            return Response()->json(["error" => "hubo un error al registrar en el id carrera".  $id .":". $ex-> getMessage()],400);
        }

    }

    public function  delete(Request $request,$id)
    {
        try{
            PersonaCarrera::find($id) -> delete();
            return response()->json([],Response::HTTP_OK);
        }catch (Exception $ex){
            return Response()->json(["error" => "hubo un error a eliminar lista idcarrera".  $id .":". $ex-> getMessage()],400);
        }

    }

}
