<?php


namespace App\Http\Middleware;
use Closure;

class Cors
{

    public function handle($request, Closure $next)
    {
        $headers =[
            "Access-Control-Allow-Origin" => "*",
            'Access-Control-Max-Age' => (60 * 60 * 24),
            'Access-Control-Allow-Methods' => $request->header('Access-Control-Request-Methods')
                ?: 'GET, HEAD, POST, PUT, PATCH, DELETE, OPTIONS',
            "Access-Control-Allow-Headers"=>  "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With",
            "Access-Control-Max-Age" => "86400"
        ];

        if ($request ->isMethod("OPTIONS"))
        {
            return response() -> json('{"method":"OPTIONS"}' , 200, $headers );

        }

        $response = $next($request);
        foreach ($headers as $key => $value )
        {
            $response ->header($key,$value);
        }
        return $response;


    }


}
