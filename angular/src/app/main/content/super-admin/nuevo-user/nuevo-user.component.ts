import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {IRole, IUser} from '../../../../api/Model';
import {UserService} from '../../../../api/user/user.service';
import {RoleService} from '../../../../api/role.service';
import {AuthenticationService} from '../../../../security/login/services/authentication.service';
import {Router} from '@angular/router';
import {MatDialog} from '@angular/material';
import {AlertDialogComponent} from '../../../../general/componentes/alert-dialog/alert-dialog.component';

@Component({
  selector: 'fuse-app-nuevo-user',
  templateUrl: './nuevo-user.component.html',
  styleUrls: ['./nuevo-user.component.scss']
})
export class NuevoUserComponent implements OnInit {

  public usuariosformgroup: FormGroup;
  public role: IRole;
  public roleArr: any = [];
  public sucursalArr: any = [];

  max_Length = 8;
  min_Length = 8;
  type = true;
  dialogRef: any;

  constructor(
    private userService: UserService,
    private roleService: RoleService,
    private authenticationService: AuthenticationService,
    private router: Router,
    public dialog: MatDialog) {
  }

  ngOnInit() {
    this.initListarFormGroup();
    this.initCombos();
  }

  initListarFormGroup() {
    this.usuariosformgroup = new FormGroup({
      'name': new FormControl('', [Validators.required]),
      'lastname': new FormControl(''),
      'email': new FormControl('', [Validators.required, Validators.email]),
      'role_id': new FormControl('', [Validators.required]),
      'password': new FormControl('', [Validators.required])
    });
  }

  initCombos() {
    this.roleService.roles().subscribe(
      data => {
        console.log('permisos', data);
        this.role = data;
        const x: any = data;
        this.roleArr = x.data;
      },
      error => {
        this.dialogs('alert-error', error.error.message, 'Upss..', error.error.subErrors, 'error');
      }
    );
  }

  save(reset: boolean) {
    const user = this.authenticationService.getUser();
    console.log(user);
    const usuario: IUser =
      {
        id: null,
        name: this.usuariosformgroup.get('name').value,
        lastname: this.usuariosformgroup.get('lastname').value,
        email: this.usuariosformgroup.get('email').value,
        password: this.usuariosformgroup.get('password').value,
        role_id: this.usuariosformgroup.get('role_id').value,
        deleted_at: null,
        updated_at: null,
        created_at: null
      };

    this.userService.save(usuario).subscribe(
      data => {
        this.dialogs('alert-succesful', 'El usuario fue creado con  éxito', '¡Éxito!', null, 'success');
        (reset === false) ? this.router.navigate(['/app/administracion']) : this.usuariosformgroup.reset();
      },
      error => {
        this.dialogs('alert-error', error.error.message, 'Upss..', error.error.subErrors, 'error');
      }
    );

    console.log(JSON.stringify(usuario));
  }

  dialogs(responseService: string, message: string, header: string, error: any[], icon ?: string, icon_color ?: string) {
    this.dialogRef = this.dialog.open(AlertDialogComponent, {
      data: {
        responseService: responseService,
        message: message,
        header: header,
        error: error,
        icon: icon
      }
    });
  }
}
