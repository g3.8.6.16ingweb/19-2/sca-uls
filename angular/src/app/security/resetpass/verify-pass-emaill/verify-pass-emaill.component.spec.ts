import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerifyPassEmaillComponent } from './verify-pass-emaill.component';

describe('VerifyPassEmaillComponent', () => {
  let component: VerifyPassEmaillComponent;
  let fixture: ComponentFixture<VerifyPassEmaillComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerifyPassEmaillComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerifyPassEmaillComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
