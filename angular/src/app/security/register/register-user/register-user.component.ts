import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {IRole, IUserRegister} from '../../../api/Model';
import {MatDialog} from '@angular/material';
import {ActivatedRoute, Router} from '@angular/router';
import {UserService} from '../../../api/user.service';
import {FuseConfigService} from '../../../../@fuse/services/config.service';
import {DomSanitizer} from '@angular/platform-browser';
import {AuthenticationService} from '../../login/services/authentication.service';
import {environment} from '../../../../environments/environment';
import {MatchPassword} from '../../../general/validators/matchPass';
import {AlertDialogComponent} from '../../../general/componentes/alert-dialog/alert-dialog.component';
import {fuseAnimations} from '../../../../@fuse/animations';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {first} from 'rxjs/operators';

@Component({
  selector: 'fuse-app-register-user',
  templateUrl: './register-user.component.html',
  styleUrls: ['./register-user.component.scss'],
  animations : fuseAnimations
})
export class RegisterUserComponent implements OnInit {

  public paises: String[] = ['Afganistan',
    'Albania',
    'Alemania',
    'Andorra',
    'Angola',
    'Antartida',
    'Antigua y Barbuda',
    'Arabia Saudi',
    'Argelia',
    'Argentina',
    'Armenia',
    'Australia',
    'Austria',
    'Azerbaiyan',
    'Bahamas',
    'Bahrain',
    'Bangladesh',
    'Barbados',
    'Belgica',
    'Belice',
    'Benin',
    'Bermudas',
    'Bielorrusia',
    'Birmania Myanmar',
    'Bolivia',
    'Bosnia y Herzegovina',
    'Botswana',
    'Brasil',
    'Brunei',
    'Bulgaria',
    'Burkina Faso',
    'Burundi',
    'Butan',
    'Cabo Verde',
    'Camboya',
    'Camerun',
    'Canada',
    'Chad',
    'Chile',
    'China',
    'Chipre',
    'Colombia',
    'Comores',
    'Congo',
    'Corea del Norte',
    'Corea del Sur',
    'Costa de Marfil',
    'Costa Rica',
    'Croacia',
    'Cuba',
    'Dinamarca',
    'Dominica',
    'Ecuador',
    'Egipto',
    'El Salvador',
    'El Vaticano',
    'Emiratos arabes Unidos',
    'Eritrea',
    'Eslovaquia',
    'Eslovenia',
    'España',
    'Estados Unidos',
    'Estonia',
    'Etiopia',
    'Filipinas',
    'Finlandia',
    'Fiji',
    'Francia',
    'Gabon',
    'Gambia',
    'Georgia',
    'Ghana',
    'Gibraltar',
    'Granada',
    'Grecia',
    'Guam',
    'Guatemala',
    'Guinea',
    'Guinea Ecuatorial',
    'Guinea Bissau',
    'Guyana',
    'Haiti',
    'Honduras',
    'Hungria',
    'India',
    'Indian Ocean',
    'Indonesia',
    'Iran',
    'Iraq',
    'Irlanda',
    'Islandia',
    'Israel',
    'Italia',
    'Jamaica',
    'Japon',
    'Jersey',
    'Jordania',
    'Kazajstan',
    'Kenia',
    'Kirguistan',
    'Kiribati',
    'Kuwait',
    'Laos',
    'Lesoto',
    'Letonia',
    'Libano',
    'Liberia',
    'Libia',
    'Liechtenstein',
    'Lituania',
    'Luxemburgo',
    'Macedonia',
    'Madagascar',
    'Malasia',
    'Malawi',
    'Maldivas',
    'Mali',
    'Malta',
    'Marruecos',
    'Mauricio',
    'Mauritania',
    'Mexico',
    'Micronesia',
    'Moldavia',
    'Monaco',
    'Mongolia',
    'Montserrat',
    'Mozambique',
    'Namibia',
    'Nauru',
    'Nepal',
    'Nicaragua',
    'Niger',
    'Nigeria',
    'Noruega',
    'Nueva Zelanda',
    'Oman',
    'Paises Bajos',
    'Pakistan',
    'Palau',
    'Panama',
    'Papua Nueva Guinea',
    'Paraguay',
    'Peru',
    'Polonia',
    'Portugal',
    'Puerto Rico',
    'Qatar',
    'Reino Unido',
    'Republica Centroafricana',
    'Republica Checa',
    'Republica Democratica del Congo',
    'Republica Dominicana',
    'Ruanda',
    'Rumania',
    'Rusia',
    'Sahara Occidental',
    'Samoa',
    'San Cristobal y Nevis',
    'San Marino',
    'San Vicente y las Granadinas',
    'Santa Lucia',
    'Santo Tome y Principe',
    'Senegal',
    'Seychelles',
    'Sierra Leona',
    'Singapur',
    'Siria',
    'Somalia',
    'Southern Ocean',
    'Sri Lanka',
    'Swazilandia',
    'Sudafrica',
    'Sudan',
    'Suecia',
    'Suiza',
    'Surinam',
    'Tailandia',
    'Taiwan',
    'Tanzania',
    'Tayikistan',
    'Togo',
    'Tokelau',
    'Tonga',
    'Trinidad y Tobago',
    'Tunez',
    'Turkmekistan',
    'Turquia',
    'Tuvalu',
    'Ucrania',
    'Uganda',
    'Uruguay',
    'Uzbekistan',
    'Vanuatu',
    'Venezuela',
    'Vietnam',
    'Yemen',
    'Djibouti',
    'Zambia',
    'Zimbabue' ];
  public usuarioFormGroup: FormGroup;
  public loader: boolean;
  public roles: IRole[] = [];
  public colorPassword = 'warn';
  public valuePassword = 0;
  public passwordMessage = '';
  dialogRef: any;
  returnUrl: string;
  error = '';
  constructor(public dialog: MatDialog,
              private route: ActivatedRoute,
              private router: Router,
              private userService: UserService,
              private fuseConfig: FuseConfigService,
              private sanitizer: DomSanitizer,
              private authenticationService: AuthenticationService,
              private httpClient: HttpClient) {
    this.fuseConfig.setConfig({
      layout: {
        navigation: 'none',
        toolbar   : 'none',
        footer    : 'none'
      }
    });
    const rolInvestigador: IRole = {id: 1, name: 'Investigador'};
    const rolUser: IRole = {id: 4, name: 'Usuario'};
    this.roles.push(rolInvestigador);
    this.roles.push(rolUser);
    this.initUSuarioFormGroup();

  }

  ngOnInit() {
    this.authenticationService.logout(true);
  }

  initUSuarioFormGroup(){
    this.usuarioFormGroup = new FormGroup({
      'password':            new FormControl('', [Validators.required, Validators.minLength(6), MatchPassword()]),
      'passwordConfirm':            new FormControl('', [Validators.required, Validators.minLength(6),  MatchPassword()]),
      'name':            new FormControl(''),
      'lastname':            new FormControl(''),
      'mail':            new FormControl('', [Validators.required, Validators.email]),
      'role':            new FormControl('', [Validators.required])
    });
  }

  compare(val1: String, val2: String){
    if (!val1 || !val2) {
      return false;
    }
    return val1 == val2;

  }
  compareObj(val1: IRole, val2: IRole){
    if (!val1 || !val2 || !val1.id || !val2.id) {
      return false;
    }
    return val1.id == val2.id;

  }

  onKey() {
    this.validarContraseña();
  }

  validarContraseña() {
    this.valuePassword = 0;
    const expRegLetrasMin = new RegExp('[a-z]+');
    const expRegLetrasMax = new RegExp('[A-Z]+');
    const expRegLetrasSim = new RegExp('[^a-zA-Z0-9]+');
    const expRegNumeros = new RegExp('[0-9]+');
    const tamanio = this.usuarioFormGroup.get('password').value.toString().length;
    if (expRegLetrasMin.test(this.usuarioFormGroup.get('password').value.toString())) {
      this.valuePassword = this.valuePassword + 20;
    }
    if (expRegLetrasMax.test(this.usuarioFormGroup.get('password').value.toString())) {
      this.valuePassword = this.valuePassword + 20;
    }
    if (expRegLetrasSim.test(this.usuarioFormGroup.get('password').value.toString())) {
      this.valuePassword = this.valuePassword + 20;
    }
    if (expRegNumeros.test(this.usuarioFormGroup.get('password').value.toString())) {
      this.valuePassword = this.valuePassword + 20;
    }
    if (tamanio >= 8) {
      this.valuePassword = this.valuePassword + 20;

    } else {
      this.valuePassword = 0;
    }
    switch (this.valuePassword) {
      case 0: {
        this.colorPassword = 'warn';
        this.passwordMessage = 'Débil';
        break;
      }
      case 20: {
        this.colorPassword = 'warn';
        this.passwordMessage = 'Débil';
        break;
      }
      case 40: {
        this.colorPassword = 'warn';
        this.passwordMessage = 'Insegura';
        break;
      }
      case 60: {
        this.colorPassword = 'primary';
        this.passwordMessage = 'Media';
        break;
      }
      case 80: {
        this.colorPassword = 'primary';
        this.passwordMessage = 'Seguro';
        break;
      }
      case 100: {
        this.colorPassword = 'accent';
        this.passwordMessage = 'Seguro';
        break;
      }
    }
  }

  dialogs(responseService: string, message: string, header: string, error: any[], errormessage: string){

    this.dialogRef = this.dialog.open(AlertDialogComponent, {
      data      : {
        responseService: responseService,
        message: message,
        header: header,
        error: error,
        errormessage: errormessage
      }});
  }

  register(){
    const userRegister: IUserRegister = {
      id: null,
      lastname: null,
      email: null,
      matchingPassword: null,
      name: null,
      password: null,
      role_id: null,
      facebook_id: null, orcid_id: null, google_id: null,
      deleted_at: null, updated_at: null, created_at: null
    };

    userRegister.password = this.usuarioFormGroup.get('password').value;
    userRegister.matchingPassword = this.usuarioFormGroup.get('passwordConfirm').value;
    userRegister.email = this.usuarioFormGroup.get('mail').value;
    userRegister.name = this.usuarioFormGroup.get('name').value;
    userRegister.lastname = this.usuarioFormGroup.get('lastname').value;
    userRegister.role_id = this.usuarioFormGroup.get('role').value.id;
    userRegister.lastname = this.usuarioFormGroup.get('lastname').value;

    this.userService.userRegister(userRegister).subscribe(
      data => {
        if (data){
          this.loader = false;
          this.dialogs('alert-succesful', 'El registro del usuario fue completado exitosamente', '¡Éxito!', null, '');
          localStorage.setItem('username_register', userRegister.name);
          this.router.navigate( ['/register/verify-email']);
        }
      },
      error => {
        this.loader = false;
        this.dialogs('alert-error', 'Ocurrió algún error en el registro de usuario.', 'Upss..', error.error.subErrors, error.error.message);
      }
    );


  }

  sessionExitosa(data){
    this.authenticationService.cargarUsuario()
      .pipe(first())
      .subscribe(
        data => {
          if (!this.returnUrl || this.returnUrl == 'undefined') { this.returnUrl = '?/app/inicio'; }
          const urls = this.returnUrl.split('?');
          const parms = {};
          if (urls.length > 1){
            const par = urls[1].split('&');
            for (let i = 0; i < par.length; i++){
              const p = par[i].split('=');
              parms[p[0]] = p[1];
            }
          }
          // this.router.navigate([urls[0]], {queryParams: parms} );
          this.router.navigate([urls[1]]);
        },
        error => {
          this.error = error;
        }
      );
  }

}
