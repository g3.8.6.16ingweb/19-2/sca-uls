import {Component, EventEmitter, Inject, OnInit, Output, ViewEncapsulation} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
  selector: 'fuse-app-alert-dialog',
  templateUrl: './dialog-locked.component.html',
  styleUrls: ['./dialog-locked.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DialogLockedComponent implements OnInit {
  constructor( public dialogRef: MatDialogRef<DialogLockedComponent>,
               @Inject(MAT_DIALOG_DATA) private data: any) {
  }

  ngOnInit() {

  }



}
