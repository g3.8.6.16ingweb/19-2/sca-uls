import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import {Role} from "../models/roles";
import {AuthenticationService} from "../services/authentication.service";
import { Location } from '@angular/common';


@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {

  constructor(private router: Router,
              private authenticationService: AuthenticationService,
              private location: Location
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    let roles: Role[] = route.data['roles'] || [];
    if(this.authenticationService.isLogin()){
      if(this.authenticationService.hasRolPermision(roles)){
        return true;
      } else {
        this.router.navigate(['/app/pages/403']);
        return false;
      }
    }

    // not logged in so redirect to login page with the return url
    this.router.navigate(['/login'], { queryParams: { returnUrl: state.url }});
    return false;
  }

}
