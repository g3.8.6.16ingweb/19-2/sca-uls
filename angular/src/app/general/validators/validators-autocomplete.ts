import {Validators, FormControl, ValidationErrors} from '@angular/forms';

export class  ValidatorsAutocomplete extends Validators{

  static requireMatch(control: FormControl): ValidationErrors | null {
    const selection: any = control.value;
    if (typeof selection == 'string') {
      return { requireMatch: true };
    }
    return null;
  }

}
